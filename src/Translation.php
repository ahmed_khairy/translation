<?php
namespace Translation;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

class Translation
{
	protected $filesystem;
	protected $lang;
	protected $PHPexcel;
	
	public function __construct()
	{
		$adapter=new Local('./lang');
		$this->PHPexcel=new \PHPExcel;
		$this->filesystem = new Filesystem($adapter);
	}

	public function setLang($lang)
	{
		$this->lang=$lang;
	}

	public function getLang()
	{
		return $this->lang;
	}

	public function buildFilePath($file_name)
	{
		return $this->getLang().'/'.$file_name;
	}

	public function saveToFile($file_name,array $data)
	{
		$file_path=$this->buildFilePath($file_name);
		if($this->filesystem->put($file_path, serialize($data)))
			return true;
		return false;
	}

	public function getFileToArray($file_name)
	{
		$file_path=$this->buildFilePath($file_name);
		if($this->filesystem->has($file_path))
			return unserialize($this->filesystem->read($file_path));
		return false;
	}

	public function getCSVToArray($csv_file)
	{
		$CSVobject=$this->loadCSV($csv_file);
		print_r($CSVobject->getActiveSheet()->toArray(null,true,true,true));
		die;
		$CSVarray=$this->CSVToArray($CSVobject);
		$formatedArray=$this->formatArray($CSVarray);
		return $formatedArray;
	}

	protected function loadCSV($csv_file)
	{
		return $CSVobject = \PHPExcel_IOFactory::load($csv_file);
	}

	protected function CSVToArray($CSVobject)
	{
		foreach ($CSVobject->getWorksheetIterator() as $worksheet) {
			$CSVarray=$worksheet->toArray();
		}
		return $CSVarray;
	}

	protected function formatArray(array $CSVarray)
	{
		$formatedArray=[];
		$arrayKeys=[];
		foreach($CSVarray[0] as $key)
		{
			if(!empty($key))
			{
				$arrayKeys[]=$key;
			}
		}
		$i=0;
		foreach($arrayKeys as $key)
		{
			foreach($CSVarray as $value)
			{
				$formatedArray[][$key]=$value[$i];
			}
			$i++;
		}

		return $formatedArray;

	}

}